# composer-package-boilerplate



## Scenario
- Human performs a commit to the composer-package repository
- A pipeline is triggered to:
  - bundle the frontend
  - commit changes to `package-release` branch
    ```
      git push --force origin release-package -o=merge_request.merge_when_pipeline_succeded. 
    ```
  - start a fake (merge request) pipeline job to trigger the `"merge_request.merge_when_pipeline_succeded"` event
- Publish the composer package to Gitlab Package Registry
    ```
      `'curl --header "Job-Token: $CI_JOB_TOKEN" --data branch=package-release "${CI_API_V4_URL}/projects/$CI_PROJECT_ID/packages/composer"'`
    ```
