import React, { lazy, Suspense } from "react";
import ReactDOM from "react-dom/client";
const App = lazy(() => import("./App"));

const rootElement = document.getElementById("root") as HTMLElement;
const root = ReactDOM.createRoot(rootElement);

const appProps = Object.assign({}, rootElement.dataset);
root.render(
  <React.StrictMode>
      <Suspense
        fallback={<div>loading</div>}
      >
        <App props={appProps} />
      </Suspense>
  </React.StrictMode>
);
