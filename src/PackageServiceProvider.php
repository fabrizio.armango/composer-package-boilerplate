<?php

namespace FabrizioArmango\ComposerPackageBoilerplate;

use Illuminate\Support\ServiceProvider;

class PackageServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register() {}

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/composer-package-boilerplate-assets
' => public_path('composer-package-boilerplate-assets')
        ]);
    }
}
