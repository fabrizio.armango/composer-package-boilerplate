const mix = require("laravel-mix");

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js("frontend/index.tsx", "public/composer-package-boilerplate-assets/js")
    .webpackConfig({
        module: {
            rules: [
                {
                    test: /\.(t|j)sx?$/,
                    exclude: /(node_modules|bower_components)/,
                    use: [
                        {
                            loader: "ts-loader",
                            options: {
                                transpileOnly: true,
                            },
                        },
                    ],
                },
            ]
        },
        resolve: {
            extensions: ["*", ".js", ".jsx", ".vue", ".ts", ".tsx", ".png"],
        }
    })
    .options({ assetModules: true })
